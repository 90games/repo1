﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Deck {

    private List<Card> deck;

    public Card topCard { get { return deck[deck.Count-1]; } }
    public int deckSize { get { return deck.Count; } }
    public int startingDeckSize = 60;

    private List<Card> possibleCards;

    public Deck()
    {
        possibleCards = new List<Card>();
        Debug.Log("About to generate a deck...");
        deck = new List<Card>();
        generateRandomDeck();
    }

    public Card draw()
    {
        if (deckSize == 0) return null;
        Card temp = topCard;
        deck.RemoveAt(deck.Count - 1);
        return temp;
    }

    public void shuffle()
    {
        for(int i=0; i<deckSize; i++)
        {
            int t = (int)((deckSize - 1) * Random.value);
            Card temp = deck[t];
            deck[t] = deck[i];
            deck[i] = temp;
        }
    }

    public void add(Card card)
    {
        int t = (int)((deckSize - 1) * Random.value);
        deck.Insert(t, card);
    }

    private void generateRandomDeck()
    {
        for (int i = 0; i < startingDeckSize; i++)
        {
            switch(Random.Range(0, 20))
            {
                case 0:
                    deck.Add(new card_SpawnBlob());
                    Debug.Log("adding a blob to deck");
                    break;
                case 1:
                    deck.Add(new card_SpawnJesus());
                    Debug.Log("adding a jesus to deck");
                    break;
                case 2:
                    deck.Add(new card_SpawnWorm());
                    break;
                case 3:
                    deck.Add(new card_SpawnTroubleshooter());
                    break;
                case 4:
                    deck.Add(new card_SpawnBaneling());
                    break;
                case 5:
                    deck.Add(new card_SpawnLag());
                    break;
                case 6:
                    deck.Add(new card_SpawnSpam());
                    break;
                case 7:
                    deck.Add(new card_SpawnJailbreak());
                    break;
                default:
                    i--;
                    break;
            }
        }
    }
}
