﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AI))]

public abstract class misc_Effect : MonoBehaviour
{

    //define in Start() of sub class
    public string effectName;

    public bool isBuff;
    public bool hasTriggered;

    //define in Start() of sub class
    public float effectOnset = 0;
    public float effectDuration;
    public float effectRepeatRate = 0;

    public AI target;

    // Use this for initialization
    protected void Start()
    {
        target = gameObject.GetComponent<AI>();
        if (target.activeEffects.Contains(this))
        {
            Destroy(this);
            return;
        }
        target.activeEffects.Add(this);
        target.modifiers.Add(effectName);
        if (effectRepeatRate > 0) InvokeRepeating("effect", effectOnset, effectRepeatRate);
        else Invoke("effect", effectOnset);
        Debug.Log(effectName + " has been applied to " + target.gameObject.name);

    }

    // Update is called once per frame
    protected void Update()
    {
        //test:
        Debug.Log("update!");
        if (target == null) target = gameObject.GetComponent<AI>();
    }

    private IEnumerator invokeEffect()
    {
        yield return new WaitForSeconds(effectOnset);
        yield return new WaitForEndOfFrame();
        Debug.Log("Applying");
        effect();
    }

    protected virtual void effect()
    {
        Debug.Log("base effect invoked");
        hasTriggered = true;
        Invoke("terminateEffect", effectDuration);
    }

    protected virtual void terminateEffect()
    {
        target.modifiers.Remove(effectName);
        target.activeEffects.Remove(this);
        Debug.Log("Removing " + effectName + " from " + target.gameObject.name);
        Destroy(this);
    }
}
