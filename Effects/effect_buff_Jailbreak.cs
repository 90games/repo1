﻿using UnityEngine;
using System.Collections;

using System.Collections
.Generic
;public class effect_buff_Jailbreak : misc_Effect {

    /// <summary>
    /// How much should the unit's attack/movespeed/etc. be buffed? 0 = do not buff, 100 = double the value and so on
    /// </summary>
    public float buffByPercentage = 100;

    private float factor;

    // Use this for initialization
    new void Start () {
        factor = buffByPercentage / 100;
        effectName = "Jailbreak";
        effectRepeatRate = 0;
        effectOnset = 0;
        effectDuration = 1;
        base.Start();
    }

    new void Update()
    {
        base.Update();
    }

    protected override void effect()
    {
        base.effect();
        target.moveSpeedMP += factor;
        target.RoFMP += factor;
        target.damageMP += factor;
    }

    protected override void terminateEffect()
    {
        base.terminateEffect();
        target.moveSpeedMP -= factor;
        target.RoFMP -= factor;
        target.damageMP -= factor;
    }
}
