﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class effect_debuff_Lag : misc_Effect
{

    public float lagFactor;

    // Use this for initialization
    new void Start()
    {
        effectName = "Lag";
        base.Start();
        effectRepeatRate = 0;
        effectOnset = 0;
    }

    protected override void effect()
    {
        base.effect();
        Debug.Log("Applying lag to " + target.name);
        target.moveSpeedMP -= lagFactor / 100;
        target.RoFMP -= lagFactor / 100;
        Debug.Log("target now has " + target.moveSpeed + " movespeed and " + target.RoF + " RoF");
    }

    protected override void terminateEffect()
    {
        target.moveSpeedMP += 0.75f;
        target.RoFMP += 0.75f;
        base.terminateEffect();
    }

}
