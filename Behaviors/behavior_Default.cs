﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;

public class behavior_Default : AI {

	// Use this for initialization
	new protected void Start () {
        this.behavior = "Default";
        base.Start();
        // this.destination = randomPointOnCircle(this.targetNode.transform.position, this.targetNode.GetComponent<NodeControl>().capRadius);
        seeker.StartPath(this.transform.position, this.destination, OnPathComplete);
        if (destination == Vector3.zero)
        {
            this.destination = randomPointOnCircle(targetNode.transform.position, targetNode.GetComponent<NodeControl>().capRadius);
            if (destination == Vector3.zero)
            {
                Debug.LogError("Destination of unit " + transform.name + " set to (0, 0, 0), even after attempted fix!");
            }
        }
        StartCoroutine(UpdatePath());
        StartCoroutine(acquireTarget());
    }

    // Update is called once per frame
    new protected void Update () {
        base.Update();
        if (destination == Vector3.zero)
        {
            this.destination = randomPointOnCircle(targetNode.transform.position, targetNode.GetComponent<NodeControl>().capRadius);
            if (destination == Vector3.zero)
            {
                Debug.LogError("Destination of unit " + transform.name + " set to (0, 0, 0), even after attempted fix!");
            }
        }

        if (this.target != null)
        {
            base.engage();
        }
        else
        {
            if (path != null)
            {
                if (path.vectorPath != null)
                {
                    if (currentWaypoint >= path.vectorPath.Count)
                    {
                        if (!pathIsEnded)
                        {
                            Debug.Log(name + " has reached end of path!");
                            pathIsEnded = true;
                        }
                    }
                    else {
                        pathIsEnded = false;
                        moveTo(path.vectorPath[currentWaypoint]);
                        if ((path.vectorPath[currentWaypoint] - transform.position).sqrMagnitude < 0.1f)
                        {
                            currentWaypoint++;
                        }
                    }
                }
            }
        }
    }

    private IEnumerator UpdatePath()
    {
        while (true)
        {
            if (destination == Vector3.zero)
            {
                Debug.LogError("Unit " + this.name + " has null destination");
                yield return false;
            }
            seeker.StartPath(this.transform.position, this.destination, OnPathComplete);
            yield return new WaitForSeconds(updateDelay + Random.value);
        }
    }

    public void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            this.path = p;
            currentWaypoint = 0;
        }
        else Debug.LogError("BAD PATH");
    }
}