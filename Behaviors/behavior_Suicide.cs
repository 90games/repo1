﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class behavior_Suicide : AI {

	// Use this for initialization
	new void Start () {
        this.behavior = "Suicide";
        base.Start();
        this.destination = randomPointOnCircle(targetNode.transform.position, targetNode.GetComponent<NodeControl>().capRadius);
    }

    // Update is called once per frame
    new void Update () {
        base.Update();
        if (destination == Vector3.zero)
        {
            Debug.LogError("Destination of unit " + transform.name + " set to (0, 0, 0)!");
            this.destination = randomPointOnCircle(targetNode.transform.position, targetNode.GetComponent<NodeControl>().capRadius);
        }

        acquireTarget();

        if (this.target != null)
        {
            base.engage();
        }

    }
}
