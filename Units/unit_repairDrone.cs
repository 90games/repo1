using UnityEngine;
using System.Collections;

public class unit_repairDrone : behavior_Default
{

    public float repairCapacity;
    public float repairRate;
    public float repairRange;
    public LayerMask unitMask;

    // Use this for initialization
    new void Start()
    {
        base.Start();
        type = "repairDrone";

        attackRangeBase = 0;
        acquisitionRangeBase = 0;
        StartCoroutine(repairCycle());


    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();

    }

    private IEnumerator repairCycle()
    {
        while (true)
        {
            Vector2 Position = new Vector2(transform.position.x, transform.position.y);
            Collider2D[] Units = Physics2D.OverlapCircleAll(Position, repairRange, unitMask);
            foreach (Collider2D coll in Units)
            {
                AI tempAI = coll.transform.parent.gameObject.GetComponent<AI>();
                if (tempAI.teamID == this.teamID && tempAI.hp < tempAI.maxhp)
                {
                    repair(tempAI);
                   // Debug.Log("I have repaired" + tempAI + "by" + repairCapacity);
                }
            }

            yield return new WaitForSeconds(repairRate);

        }
    }

    public void repair(AI friend)

    {

        if ((friend.hp + repairCapacity) > friend.maxhp)
        {
            friend.hp += (friend.maxhp - friend.hp);
        }
        else
        {
            friend.hp += repairCapacity;
        }

    }
}