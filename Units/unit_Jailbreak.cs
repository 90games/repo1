﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//TODO: implement buff

public class unit_Jailbreak : behavior_Default
{

    private string buffName = "allAroundBuff";
    private float radius = 10;

    [Tooltip("The factor (percentage) by which this unit will buff others")]
    public float buffFactor = 100;


    // Use this for initialization
    new void Start()
    {
        base.Start();
        type = "Jailbreak";
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
        buffAlliesInRadius();
    }

    private void buffAlliesInRadius()
    {
        /*
        foreach (List<GameObject> list in GameController.teamLists)
        {
            foreach (GameObject unit in list)
            {
                if (unit.GetComponent<AI>().teamID == this.teamID)
                {
                    if ((this.transform.position - unit.transform.position).sqrMagnitude < Mathf.Pow(radius, 2))
                    {
                        applyBuff(unit);
                    }
                }
            }
        }
        */
        //Optionally use this code:
        
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, radius, GameController.units);
        foreach(Collider2D col in cols)
        {
            if (col.transform.parent.gameObject.GetComponent<AI>().teamID == this.teamID) applyBuff(col.transform.parent.gameObject);
        }
        
    }

    private void applyBuff(GameObject unit)
    {
        if (unit.GetComponent<effect_buff_Jailbreak>() == null)
        {
            effect_buff_Jailbreak newJBreak = unit.AddComponent<effect_buff_Jailbreak>();
            newJBreak.buffByPercentage = this.buffFactor;
        }
    }
}