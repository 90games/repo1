﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class unit_Blob : behavior_Default {

    public int deathsChecked;
    public float absorbRadius = 3;
    public float absorbFactor= 0.2f;

    // Use this for initialization
    new void Start () {
        deathsChecked = 0;
        base.Start();
        type = "Blob";
    }
	
	// Update is called once per frame
	new void Update () {
        base.Update();
        if (GameController.killBoard.Count > deathsChecked)
        {
            for (; deathsChecked < GameController.killBoard.Count; deathsChecked++)
            {
                deathLog tempLog = GameController.killBoard[deathsChecked];
                if ((tempLog.positionOnDeath - this.transform.position).magnitude <=  absorbRadius && tempLog.teamID!=this.teamID)
                {
                    absorbHealth(tempLog.maxhp);
                }
            }
            
        }
	}

    private void absorbHealth(float bonusHP)
    {
        hp += bonusHP * absorbFactor;
    }
}
