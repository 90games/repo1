﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//TODO: implement squad mechanics
public class unit_Baneling10 : type_Squad {

    bool rush = false;

    // Use this for initialization
    new void Start () {
        type = "Baneling";
        base.Start();
        if (range(targetNode) < acquisitionRange && localTargets.Count == 0) target = targetNode; 
    }

    // Update is called once per frame
    new void Update () {
        base.Update();

        if (destination == Vector3.zero)
        {
            Debug.LogError("Destination of unit " + transform.name + " set to (0, 0, 0)!");
            this.destination = randomPointOnCircle(targetNode.transform.position, targetNode.GetComponent<NodeControl>().capRadius);
        }

        if (this.target != null)
        {
            engage();
        }
	}

    public override bool engage()
    {
        destination = target.transform.position;
        if (!base.engage())
        {
            if (!rush)
            {
                moveSpeedMP += 2;
                rush = true;
            }
            return false;
        }
        //if(range()<attackRange)
        //{
        //    Debug.Log("Suicide!");
        //    if (getTypeString(target) == "Unit")
        //    {
        //        Debug.Log("Dealing damage and suiciding now");
        //        target.GetComponent<AI>().doDamage(damage);
        //        onDeath();
        //        return true;
        //    }
        //    if (getTypeString(target) == "node" && 
        //        target.GetComponent<NodeControl>().ownerTeamID != teamID)
        //    {
        //        target.GetComponent<NodeControl>().ownership -= 10;
        //        onDeath();
        //        return true;
        //    }
        //}
        return false;
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {

        if (coll.gameObject == target)
        {
            Debug.Log("Dealing damage and suiciding now");
            target.GetComponent<AI>().doDamage(damage);
            Debug.Log(name + "suiciding");
            onDeath();
        }
    }

}
