﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class deathLog {

    public string name;
    public string type;
    public float maxhp;
    public float maxLifeTime;
    public int teamID;

    public Vector3 positionOnDeath;
    public float hpOnDeath;
    public float lifeTimeOnDeath;
    public AI killedBy;
    public AI[] assists;
    public float timeOfDeath;
}
