﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

//TODO: organize variables here
//TODO: seriously, organize variables in here

public class GameController : MonoBehaviour {

    //teamLists keep track of all units on the map sorted into various teams
    //these are static so you can use them via GameController.teamLists[teamID][unitID]

    public static int _teams=2;
    public int __teams;

    /// <summary>
    /// Manually input the number of non-neutral teams in the scene (very important!!)
    /// </summary>
    public static int teams { get { return _teams; } }

    public static Vector2 mouseCurrent
    {
        get
        {
            Vector3 v = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            v.z = 0;
            return v;
        }
    }

    public static List<List<GameObject>> teamLists = new List<List<GameObject>>();
    public static List<int> ticketList = new List<int>();
    public static List<GameObject> bases = new List<GameObject>();
    public static List<GameObject> nodes = new List<GameObject>();
    public static List<Player> playerList;

    /// <summary>
    /// How much energy does each player obtain every tick?
    /// </summary>
    public int energyPerTick = 1;
    
    /// <summary>
    /// How much time (in seconds) passes between ticks?
    /// </summary>
    public int tickDelay = 1;

    /// <summary>
    /// What is the maximum amount of energy a player can have?
    /// </summary>
    public int eMax = 10;

    /// <summary>
    /// Maximum number of cards a player can hold at any given time
    /// </summary>
    public int maxCards = 5;

    public float cameraMoveSpeed = 100;
    private Camera cam;
    private Vector3 camTranslate;
    public List<GameObject> nodesPublic;
    public float FPS;
    public static List<deathLog> killBoard;
    public GameObject prefabTest;
    public GameObject spawnPointTest;
    public Text eDisplay;
    public GameObject cardTest;
    public static LineRenderer lRenderer;

    public float worldUnitsPerScreenPixel { get { return (Camera.main.orthographicSize * 2f) / Camera.main.pixelHeight; } }
    public float cameraWidth { get { return worldUnitsPerScreenPixel * Camera.main.pixelWidth; } }
    public float cameraHeight { get { return worldUnitsPerScreenPixel * Camera.main.pixelHeight; } }

    public static LayerMask units;

    //Temporary:
    public List<GameObject> _allCardPrefabs = new List<GameObject>();
    public static List<GameObject> allCardPrefabs = new List<GameObject>();
    public int canary = 0;

    void Awake()
    {
        playerList = new List<Player>();
        allCardPrefabs = _allCardPrefabs;
        units = LayerMask.GetMask("Units");
    }

    // Use this for initialization
    void Start () {
        int j = 0;
        for(int i=0; i< teams; i++)
        {
            string teamName = "Team" + i;
            GameObject[] temp = GameObject.FindGameObjectsWithTag(teamName);
            List<GameObject> temp2 = new List<GameObject>();
            foreach(GameObject item in temp)
            {
                temp2.Add(item);
            }
            teamLists.Add(temp2);
            ticketList.Add(j);
        }
        GameObject[] temp1 = GameObject.FindGameObjectsWithTag("Node");
        if (temp1.Length == 0) Debug.LogError("No nodes found. ABORT! ABORT!");
        foreach (GameObject item in temp1)
        {
            nodes.Add(item);
        }
        killBoard = new List<deathLog>();
        cam = Camera.main;
        camTranslate = new Vector3();
        StartCoroutine(Tick());
        Physics2D.queriesHitTriggers = true;
        lRenderer = GetComponent<LineRenderer>();
        StartCoroutine(LateStart());
    }

    private IEnumerator LateStart()
    {
        yield return new WaitForEndOfFrame();
        energyPerTick = Settings.energyPerTick;
        tickDelay = Settings.tickRate;

    }

    // Update is called once per frame
    void Update () {
        __teams = playerList.Count;
        FPS = 1 / Time.deltaTime;
        foreach (List<GameObject> item in teamLists) filterList(item);
        nodesPublic = nodes;
        camTranslate.x = Input.GetAxis("Horizontal");
        camTranslate.y = Input.GetAxis("Vertical");
        cam.transform.Translate(camTranslate * cameraMoveSpeed * Time.deltaTime);
        eDisplay.text = "";
        for (int i=0; i<playerList.Count; i++)
        {
            eDisplay.text += "Player " + i + " energy: " + playerList[i].energy + Environment.NewLine;
        }
        //sort playerList:
        for(int i=0; i<playerList.Count; i++)
        {
            int teamIDtemp = playerList[i].teamID;
            Player temp = playerList[teamIDtemp];
            playerList[teamIDtemp] = playerList[i];
            playerList[i] = temp;

        }

        debugger();
    }

    private void debugger()
    {
    }

    private IEnumerator Tick()
    {
        while (true)
        {
            for (int i = 0; i < playerList.Count; i++)
            {
                Debug.Log("Adding energy to team " + i);
                if (playerList[i].energy + energyPerTick <= eMax) playerList[i].energy += energyPerTick;
                else playerList[i].energy = eMax;
            }
            yield return new WaitForSeconds(tickDelay);
        }
    }

    public static GameObject getPrefab<T>(){
        foreach(GameObject obj in allCardPrefabs)
        {
            if (obj.GetComponent<T>() != null) return obj;
        }
        return null;
    }

public static void filterList<T>(List<T> list)
    {
        if (list.Count == 0) return;
        for(int i=0; i<list.Count; i++)
        {
            if (list[i] == null) list.RemoveAt(i);
        }
    }

    public void tempEnableAggro()
    {
        foreach (GameObject unit in teamLists[0]) unit.GetComponent<AI>().selectByAggro = true;
    }

    public void tempDisableAggro()
    {
        foreach (GameObject unit in teamLists[0]) unit.GetComponent<AI>().selectByAggro = false;
    }

    public void writeDeathLog()
    {
        foreach(deathLog log in killBoard)
        {
            Debug.Log("name: " + log.name);
            Debug.Log("killed by: " + log.killedBy);
            Debug.Log("time of death: " + log.timeOfDeath);
            Debug.Log("assists: " + log.assists.ToString());
        }
    }

    private void applyBuffTest()
    {
        Debug.Log("hello");
        GameObject someUnit = GameObject.FindGameObjectWithTag("Team0");
        AI someAI = someUnit.GetComponent<AI>();
        Debug.Log("Applying Lag to " + someUnit.name);
        someUnit.AddComponent<effect_debuff_Lag>();
    }

    public void SpawnUnitTest()
    {
        AI.spawn(spawnPointTest.transform.position, 1, 1, 4, 10, 1, 100, 0, 0, 100, new Vector3(1, 1, 1), prefabTest);
    }

    
}
